import java.util.Scanner;

public class Fibonachi {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите n-ое число Фибоначчи: ");
        long num = scanner.nextInt();
        if (num < 0) {
            System.out.println("Введите целое число");
            return;
        }
        long before = System.currentTimeMillis();
        System.out.println(fibonacci(num));
        long after = System.currentTimeMillis();
        System.out.println(after - before + " миллисекунд - время работы обычного метода");
        long beforeRecursion = System.currentTimeMillis();
        System.out.println(fibonacciRecursion(num));
        long afterRecursion = System.currentTimeMillis();
        System.out.println(afterRecursion - beforeRecursion + " миллисекунд - время работы рекурсивного метода");

    }

    private static int fibonacci(long num) {
        if (num == 0){
            return 0;
        }
        if (num == 1){
            return 1;
        }
        int First = 1;
        int Second = 1;
        int Number = 1;
        for (int i = 3; i < num+1; i++) {
            Number = First + Second;
            First = Second;
            Second = Number;
        }
        return Number;
    }

    private static int fibonacciRecursion(long num) {
        if (num == 0) {
            return 0;
        }
        if (num == 1) {
            return 1;
        }
        return fibonacciRecursion(num-1)+ fibonacciRecursion(num-2);
    }
}

